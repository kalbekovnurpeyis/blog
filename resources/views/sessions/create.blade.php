@extends('layouts.layout')

@section('content')
    <h3 class="pb-3 mb-4 font-italic border-bottom">Sign In</h3>
    <form method="post" action="/login">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" class="form-control" id="email" name="email" required>
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Log In</button>
        </div>

        @include('layouts.errors')
    </form>
@endsection