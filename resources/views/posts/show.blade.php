@extends ('layouts.layout')

@section('content')
    <div class="blog-post">
        <h2 class="blog-post-title">{{ $post->title }}</h2>
        <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString() }} by <a href="#">{{ $post->user->name }}</a></p>
        <p>{{ $post->body }}</p>
        <div class="comments">
            <ul class="list-group">
                @foreach($post->comments as $comment)
                <li class="list-group-item">
                    <strong>
                        {{ $comment->created_at->diffForHumans() }}:
                    </strong>
                    {{ $comment->body }}
                </li>
                @endforeach
            </ul>
            <hr>
            {{--<div class="card">--}}
                <div class="card-block">
                    <form method="post" action="/posts/{{ $post->id }}/comments">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <textarea name="body" class="form-control" placeholder="Your comment here" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add Comment</button>
                        </div>
                    </form>
                    @include('layouts.errors')
                </div>
            {{--</div>--}}
        </div>
    </div><!-- /.blog-post -->
@endsection